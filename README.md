# README #

Ever had problems populating a Coherence cache using a Cache Loader. This implementation allows for each storage member to quickly restore data which pertains only to that particular member.
The source contains a reference implementation using BDB 5.0 that is proven to work in production environments with 100 GB datasets.

### What is this repository for? ###

* A Partitioned Read Write Backing Map implementation and a reference BDB CacheStore. 
* 1.1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Modify your caching configuration, add the backing map to the configuration which you want to persist. Add the jar to the class path.

* Configuration

```xml
<?xml version="1.0" encoding="UTF-8"?>
<cache-config
        xmlns="http://xmlns.oracle.com/coherence/coherence-cache-config"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://xmlns.oracle.com/coherence/coherence-cache-config coherence-cache-config.xsd">
    <caching-scheme-mapping>
        <cache-mapping>
            <cache-name>*</cache-name>
            <scheme-name>custom-rwbm</scheme-name>
        </cache-mapping>
    </caching-scheme-mapping>
    <caching-schemes>
        <distributed-scheme>
            <scheme-name>custom-rwbm</scheme-name>
            <thread-count>10</thread-count>
            <backing-map-scheme>
                <read-write-backing-map-scheme>

                    <class-name>
                        store.PartitionedReadWriteBackingMap
                    </class-name>

                    <internal-cache-scheme>
                        <local-scheme/>
                    </internal-cache-scheme>

                    <cachestore-scheme>

                        <class-scheme>
                            <class-name>store.bdb.BdbCacheStore</class-name>
                            <init-params>
                                <init-param>
                                    <param-type>com.tangosol.net.BackingMapManagerContext</param-type>
                                    <param-value>{manager-context}</param-value>
                                </init-param>
                                <init-param>
                                    <param-type>java.lang.String</param-type>
                                    <param-value>{cache-name}</param-value>
                                </init-param>
                                <init-param>
                                    <param-type>java.lang.String</param-type>
                                    <param-value system-property="store.location">/var/tmp/bdb</param-value>
                                </init-param>
                            </init-params>
                        </class-scheme>
                    </cachestore-scheme>
                    <write-delay>10s</write-delay>
                </read-write-backing-map-scheme>
            </backing-map-scheme>
            <autostart>true</autostart>
        </distributed-scheme>
    </caching-schemes>
</cache-config>
```

* Dependencies
In this particular implementation the only dependency is on BDB, that can be downloaded from [http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/downloads/index.html]

* Database configuration
For the reference implementation, the location where the BDB files are kept can be controlled directly by specifying the 3rd parameter to the cache store or by specifying the _store.location_ property.

* How to run tests

* Deployment instructions
- Add the jar to the class path
- Modify the cache configuration

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* chelin@icloud.com