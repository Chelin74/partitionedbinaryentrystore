/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import com.sleepycat.je.DatabaseNotFoundException;
import com.tangosol.net.BackingMapManagerContext;
import com.tangosol.net.partition.PartitionSet;
import store.utils.Utils;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;

public class BdbCacheStore
        extends AbstractBdbCacheStore
        implements StorageMBean {

    /**
     * The directory where all the partitioned bdb files should be stored.
     */
    private final Path path;

    public BdbCacheStore(BackingMapManagerContext context, String name, String path) {
        super(name, context);
        this.path = FileSystems.getDefault().getPath(path);

        registerMBean();
    }

    @Override
    public void destroy() {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        try {
            server.unregisterMBean(getMBeanName());
        } catch (InstanceNotFoundException | MBeanRegistrationException | MalformedObjectNameException e) {
            logger.error("Failed to de-register MBean", e);
        }
    }

    @Override
    public boolean remove(int partition) {
        Utils.StoreOp operation = Utils.StoreOp.STORE;

        try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
            try {
                if (env.getEnvironment().getDatabaseNames().contains(getName())) {
                    env.getEnvironment().removeDatabase(null, getName());
                }
                return true;
            } catch (DatabaseNotFoundException e) {
                // no such database, we are open in read only mode therefore there may be the case
                // that there are no data here yet.
                logger.error("Database '" + getName() + "' not found for partition " + partition + ".");
            } catch (Throwable t) {
                logger.error("Unable to remove partition", t);
            }
        } finally {
            unlockEnvironment(partition, operation);
        }
        return false;
    }

    @Override
    public String toString() {
        return "NuevoBdbCacheStore{" +
               "managerContext=" + getManagerContext() +
               ", name='" + getName() + '\'' +
               ", path=" + path +
               ", environmentLock=" + environmentLock +
               '}';
    }


    private void registerMBean() {
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();

        try {
            ObjectName name = getMBeanName();
            server.registerMBean(this, name);
        } catch (MalformedObjectNameException | NotCompliantMBeanException | InstanceAlreadyExistsException | MBeanRegistrationException e) {
            logger.error("Unable to register MBean", e);
        }
    }

    private ObjectName getMBeanName()
            throws MalformedObjectNameException {
        return new ObjectName(getClass().getCanonicalName() + ":type=" + getName());
    }

    @Override
    public String[] getDatabaseNames() {
        Collection<String> databaseNames = getDatabaseNamesInternal();
        return databaseNames.toArray(new String[databaseNames.size()]);
    }

    @Override
    public void remove() {
        PartitionSet partitions = getOwnedPartitions();
        for (int partition = partitions.next(0); partition != -1; partition = partitions.next(partition + 1)) {
            remove(partition);
        }
    }

    @Override
    public void unlockAll() {

    }

    @Override
    public String[] getOrphanedDatabases() {
        Set<String> cacheNames = getCacheNamesInternal();
        Collection<String> databaseNames = getDatabaseNamesInternal();
        databaseNames.removeAll(cacheNames);
        return databaseNames.toArray(new String[databaseNames.size()]);
    }

    @Override
    public void removeOrphans(String[] excludes) {
        Set<String> cacheNames = getCacheNamesInternal();
        Collection<String> databaseNames = getDatabaseNamesInternal();
        databaseNames.removeAll(cacheNames);

        for (Iterator<String> remaining = databaseNames.iterator(); remaining.hasNext(); ) {
            String database = remaining.next();

            for (String exclude : excludes) {
                if (database.contains(exclude)) {
                    logger.info("database {" + database + "} excluded from deletion");
                    remaining.remove();
                    break;
                }
            }
        }

        logger.info("Pruning databases: " + databaseNames);
        for (String database : databaseNames) {
            removeOwnedDatabase(database);
        }
    }

    @Override
    public void removeDatabase(String database) {
        PartitionSet partitions = getAllPartitions();
        deleteDatabase(database, partitions);
    }

    @Override
    public void removeOwnedDatabase(String database) {
        PartitionSet partitions = getOwnedPartitions();
        deleteDatabase(database, partitions);
    }

    /**
     * Obliterate the specified <tt>database</tt> for the given <tt>partitions</tt>.
     *
     * @param database   the name of the database to delete
     * @param partitions the {@link PartitionSet} to remove the specified database for
     */
    private void deleteDatabase(String database, PartitionSet partitions) {
        for (int partition = partitions.next(0); partition != -1; partition = partitions.next(partition + 1)) {
            Utils.StoreOp operation = Utils.StoreOp.REMOVE;

            try (EnvironmentTuple environment = ensureEnvironment(operation, partition)) {
                if (environment.getEnvironment() == null) {
                    continue;
                }

                if (environment.getEnvironment().getDatabaseNames().contains(database)) {
                    environment.getEnvironment().removeDatabase(null, database);
                }
            } catch (DatabaseNotFoundException e) {
                // do nothing
            } finally {
                unlockEnvironment(partition, operation);
            }
        }
    }

    public String[] getCacheNames() {
        Set<String> cacheNames = getCacheNamesInternal();
        return cacheNames.toArray(new String[cacheNames.size()]);
    }

    @SuppressWarnings("unchecked")
    private Set<String> getCacheNamesInternal() {
        Enumeration<String> cacheNameEnum = getManagerContext().getCacheService().getCacheNames();

        Set<String> names = new HashSet<>();

        while (cacheNameEnum.hasMoreElements()) {
            names.add(cacheNameEnum.nextElement());
        }

        return names;
    }


    private Collection<String> getDatabaseNamesInternal() {
        Set<String> names = new HashSet<>();

        PartitionSet partitions = getAllPartitions();
        for (int partition = partitions.next(0); partition != -1; partition = partitions.next(partition + 1)) {
            Utils.StoreOp operation = Utils.StoreOp.LOAD;
            try (EnvironmentTuple environment = ensureEnvironment(operation, partition)) {
                operation = environment.getStorageOp();

                if (environment.getEnvironment() == null) {
                    continue;
                }

                names.addAll(environment.getEnvironment().getDatabaseNames());
            } finally {
                unlockEnvironment(partition, operation);
            }
        }
        return names;
    }

    @Override
    protected void onPartitionBroken(int partition) {
        removePath(getPath(partition));
    }

    @Override
    protected Path getPath(int partition) {
        return path.resolve(String.valueOf(partition));
    }
}
