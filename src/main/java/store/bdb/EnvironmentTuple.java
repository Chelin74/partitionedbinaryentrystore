/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import com.sleepycat.je.Environment;
import store.utils.Utils;

/**
 * @author charlie.helin@gmail.com
 */
public class EnvironmentTuple
        implements AutoCloseable {

    private final Environment environment;
    private final Utils.StoreOp storageOp;

    public EnvironmentTuple(Environment environment, Utils.StoreOp storageOp) {
        this.environment = environment;
        this.storageOp = storageOp;
    }

    @Override
    public void close() {
        if (environment != null && environment.isValid()) {
            getEnvironment().close();
        }
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Utils.StoreOp getStorageOp() {
        return storageOp;
    }
}
