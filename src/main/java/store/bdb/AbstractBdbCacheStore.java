/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import com.sleepycat.je.*;
import com.tangosol.coherence.component.net.Member;
import com.tangosol.coherence.component.util.daemon.queueProcessor.service.grid.PartitionedService;
import com.tangosol.net.BackingMapManagerContext;
import com.tangosol.net.GuardSupport;
import com.tangosol.net.partition.PartitionSet;
import com.tangosol.util.Binary;
import com.tangosol.util.BinaryEntry;
import org.apache.log4j.Logger;
import store.PartitionedBinaryEntryStore;
import store.utils.Utils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static store.utils.Utils.groupBinaryEntries;

/**
 * @author charlie.helin@gmail.com
 */
public abstract class AbstractBdbCacheStore
        implements PartitionedBinaryEntryStore {

    protected final static DatabaseConfig READONLY_CONFIG = getROConfig();
    protected final static DatabaseConfig READWRITE_CONFIG = getRWConfig();
    protected final Logger logger;
    /**
     * A map that will ensure that no conflicting environment is opened.
     */
    protected final ConcurrentHashMap<Integer, ReadWriteLock> environmentLock = new ConcurrentHashMap<>();
    private final BackingMapManagerContext managerContext;
    /**
     * The name of the cache.
     */
    private final String name;

    protected AbstractBdbCacheStore(String name, BackingMapManagerContext managerContext) {
        this.managerContext = managerContext;
        this.name = name;
        logger = Logger.getLogger(getClass().getSimpleName() + " [" + name + "]");

    }

    protected static DatabaseConfig getROConfig() {
        DatabaseConfig storeConfig = new DatabaseConfig();
        storeConfig.setAllowCreate(false);
        storeConfig.setReadOnly(true);
        storeConfig.setTransactional(false);
        return storeConfig;
    }

    protected static DatabaseConfig getRWConfig() {
        DatabaseConfig storeConfig = new DatabaseConfig();
        storeConfig.setAllowCreate(true);
        storeConfig.setReadOnly(false);
        storeConfig.setTransactional(false);
        return storeConfig;
    }

    protected EnvironmentConfig ensureEnvironmentConfig(Utils.StoreOp operation) {
        EnvironmentConfig envConfig = new EnvironmentConfig();

        envConfig.setAllowCreate(operation == Utils.StoreOp.STORE);
        envConfig.setReadOnly(operation == Utils.StoreOp.LOAD);
        envConfig.setCacheSize(1000000);

        return envConfig;
    }

    protected EnvironmentTuple ensureEnvironment(Utils.StoreOp operation, int partition) {
        Path file = getPath(partition);
        boolean exists = Files.isDirectory(file);
        lockEnvironment(partition, operation);

        switch (operation) {
            case LOAD:
            case REMOVE:
                if (!exists) {
                    return new EnvironmentTuple(null, operation);
                }

            case STORE:

                if (!exists) {
                    try {
                        Files.createDirectories(file);

                    } catch (IOException e) {
                        logger.error("Cannot create storage for " + partition, e);
                    }
                }

                if (!exists && Files.exists(file)) {
                    logger.debug("Generated " + file + " for storage of partition " + partition);
                }
        }

        try {
            return new EnvironmentTuple(new Environment(file.toFile(), ensureEnvironmentConfig(operation)), operation);
        } catch (EnvironmentNotFoundException exception) {
            upgradeLock(operation, partition);

            // Environment is corrupt, warn about this fact but
            logger.error("Environment for partition "
                         + partition
                         + " is corrupted and will be opened write create access");

            return new EnvironmentTuple(new Environment(file.toFile(), ensureEnvironmentConfig(Utils.StoreOp.STORE)),
                                               Utils.StoreOp.STORE);
        } catch (EnvironmentFailureException e) {
            logger.error("Environment for partition " + partition + " is corrupted, attempting repair", e);
            try {
                upgradeLock(operation, partition);
                return new EnvironmentTuple(new Environment(file.toFile(),
                                                                   ensureEnvironmentConfig(Utils.StoreOp.STORE)),
                                                   Utils.StoreOp.STORE);
            } catch (Exception newException) {
                logger.error("Environment is beyond repair, removing");
                onPartitionBroken(partition);
                upgradeLock(operation, partition);
                return new EnvironmentTuple(new Environment(file.toFile(), ensureEnvironmentConfig(operation)),
                                                   Utils.StoreOp.STORE);
            }
        } catch (IllegalArgumentException e) {
            logger.warn("Attempting to open a read write environment, using a read only config", e);
            upgradeLock(operation, partition);
            return new EnvironmentTuple(new Environment(file.toFile(), ensureEnvironmentConfig(Utils.StoreOp.STORE)),
                                               Utils.StoreOp.STORE);
        }
    }

    protected abstract void onPartitionBroken(int partition);

    private void upgradeLock(Utils.StoreOp operation, int partition) {
        if (operation == Utils.StoreOp.LOAD) {
            unlockEnvironment(partition, operation);
            lockEnvironment(partition, Utils.StoreOp.STORE);
        }
    }

    protected abstract Path getPath(int partition);

    @Override
    public String getName() {
        return name;
    }

    /**
     * Get the path/url of the partition.
     *
     * @param partition the partition to resolve
     * @return a String representing the path/url
     */
    protected final String getPartitionPath(int partition) {
        return getPath(partition).toString();
    }

    /**
     * Locks the capabilities to create a conflicting environment for the specified <tt>partition</tt>
     * and <tt>operation</tt>.
     *
     * @param partition the partition to lock
     * @param operation the operation
     */
    protected void lockEnvironment(int partition, Utils.StoreOp operation) {
        ReadWriteLock lock = environmentLock.get(partition);

        if (lock == null) {
            ReadWriteLock prev = environmentLock.putIfAbsent(partition, lock = new ReentrantReadWriteLock());
            lock = prev == null ? lock : prev;
        }

        try {
            switch (operation) {
                case LOAD:
                    while (!lock.readLock().tryLock(200, TimeUnit.MILLISECONDS)) {
                        GuardSupport.heartbeat();
                        logger.warn("Waiting for read access " + partition);
                    }
                    break;
                default:
                    while (!lock.writeLock().tryLock(200, TimeUnit.MILLISECONDS)) {
                        GuardSupport.heartbeat();
                        logger.warn("Waiting for write access " + partition);
                    }
            }
        } catch (InterruptedException e) {
            logger.error("Operation was interrupted", e);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Unlocks the capability to instantiate a new environment for the specified <tt>operation</tt>.
     *
     * @param partition the partition to unlock the environment for
     * @param operation the type of operation
     */
    protected void unlockEnvironment(int partition, Utils.StoreOp operation) {
        ReadWriteLock lock = environmentLock.get(partition);

        if (lock == null) {
            logger.error("Partition " + partition + " not locked for " + operation);
            return;
        }

        try {
            switch (operation) {
                case LOAD:
                    lock.readLock().unlock();
                    break;
                default:
                    lock.writeLock().unlock();
            }
        } catch (IllegalMonitorStateException e) {
            logger.error("Partition " + partition + " not locked for " + operation, e);
        }
    }

    protected PartitionSet getAllPartitions() {
        PartitionedService cacheService = (PartitionedService) getManagerContext().getCacheService();
        cacheService.getPartitionCount();
        PartitionSet partitionSet = new PartitionSet(cacheService.getPartitionCount());
        partitionSet.fill();
        return partitionSet;
    }

    protected PartitionSet getOwnedPartitions() {
        PartitionedService cacheService = (PartitionedService) getManagerContext().getCacheService();
        Member thisMember = cacheService.getThisMember();
        return cacheService.getOwnedPartitions(thisMember);
    }

    @Override
    public Set<Binary> getKeys(int partition) {
        Utils.StoreOp operation = Utils.StoreOp.LOAD;

        try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
            operation = env.getStorageOp();

            if (env.getEnvironment() == null) {
                return Collections.emptySet();
            }

            try (Database database = env.getEnvironment().openDatabase(null, getName(), READONLY_CONFIG);
                 Cursor cursor = database.openCursor(null, null)) {

                if (logger.isDebugEnabled()) {
                    logger.debug("Getting keys from " + database.getEnvironment().getHome().getAbsolutePath());
                }

                Set<Binary> keys = new HashSet<>();

                BinaryWrapper key = new BinaryWrapper();
                BinaryWrapper value = new BinaryWrapper();

                // do not read the value, only the key
                value.setPartial(0, 0, true);

                while (cursor.getNext(key, value, LockMode.READ_UNCOMMITTED) == OperationStatus.SUCCESS) {
                    keys.add(key.getBinary());
                }

                if (logger.isDebugEnabled()) {
                    logger.debug("Retrieved " + keys.size() + " keys from partition " + partition + " ("
                                 + database.getEnvironment().getHome().getAbsolutePath() + ")");
                }
                return keys;
            } catch (DatabaseNotFoundException e) {
                // no such database, we are open in read only mode therefore there may be the case
                // that there are no data here yet.
                return Collections.emptySet();
            } catch (Throwable t) {
                logger.error("Unable to retrieve keys from " + getPartitionPath(partition), t);
                throw t;
            }
        } finally {
            unlockEnvironment(partition, operation);
        }
    }

    public abstract boolean remove(int partition);

    @Override
    public void load(BinaryEntry binaryEntry) {
        Utils.StoreOp operation = Utils.StoreOp.LOAD;
        int partition = getPartitionForEntry(binaryEntry);

        try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
            operation = env.getStorageOp();

            if (env.getEnvironment() == null) {
                return;
            }

            try (Database database = env.getEnvironment().openDatabase(null, getName(), READONLY_CONFIG)) {

                BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());
                BinaryWrapper value = new BinaryWrapper();

                OperationStatus operationStatus = database.get(null, key, value, LockMode.DEFAULT);
                if (operationStatus == OperationStatus.SUCCESS) {
                    binaryEntry.updateBinaryValue(value.getBinary());
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("load unable to retrieve "
                                     + binaryEntry.getKey()
                                     + " from partition "
                                     + partition);
                    }
                }
            } catch (DatabaseNotFoundException e) {
                // no such database, we are open in read only mode therefore there may be the case
                // that there are no data here yet.
            } catch (Throwable t) {
                logAndThrow("load", "from", binaryEntry, partition, t);
            }
        } finally {
            unlockEnvironment(partition, operation);
        }
    }

    @Override
    public void loadAll(Set set) {

        @SuppressWarnings("unchecked")
        Map<Integer, Collection<BinaryEntry>> entriesByPartition = groupBinaryEntries((Set<BinaryEntry>) set,
                                                                                             getManagerContext());

        for (Map.Entry<Integer, Collection<BinaryEntry>> partitionEntries : entriesByPartition.entrySet()) {
            int partition = partitionEntries.getKey();
            Utils.StoreOp operation = Utils.StoreOp.LOAD;
            try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
                operation = env.getStorageOp();

                if (env.getEnvironment() == null) {
                    // no shard exists for the partition; continue
                    continue;
                }

                try (Database database = env.getEnvironment().openDatabase(null, getName(), READONLY_CONFIG)) {
                    loadEntries(partitionEntries.getValue(), database);
                } catch (DatabaseNotFoundException e) {
                    // no such database, we are open in read only mode therefore there may be the case
                    // that there are no data here yet.
                }

            } finally {
                GuardSupport.heartbeat();
                unlockEnvironment(partition, operation);
            }
        }
    }

    /**
     * Loads the specified <tt>entries</tt> that has been grouped by the partition from the
     * passed in <tt>database</tt>.
     *
     * @param entries  the {@link BinaryEntry entries} to load
     * @param database the Database to load the entries from
     */
    private void loadEntries(Collection<BinaryEntry> entries, Database database) {
        int counter = 0;

        if (logger.isDebugEnabled()) {
            logger.debug("Loading: " + entries.size() + " from " + database.getEnvironment().getHome()
                                                                           .getAbsolutePath());
        }

        for (BinaryEntry binaryEntry : entries) {
            BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());
            BinaryWrapper value = new BinaryWrapper();

            try {
                if (database.get(null, key, value, LockMode.READ_UNCOMMITTED) == OperationStatus.SUCCESS) {
                    binaryEntry.updateBinaryValue(value.getBinary());
                    counter++;
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("loadAll failed to retrieve "
                                     + binaryEntry.getKey()
                                     + " from partition "
                                     + getPartitionForEntry(binaryEntry));
                    }
                }
            } catch (Throwable t) {
                logAndThrow("loadAll", "from", binaryEntry, getPartitionForEntry(binaryEntry), t);
            }
        }

        logger.debug("Loaded: " + counter + " from " + database.getEnvironment().getHome().getAbsolutePath());
    }

    @Override
    public void store(BinaryEntry binaryEntry) {
        int partition = getPartitionForEntry(binaryEntry);
        Utils.StoreOp operation = Utils.StoreOp.STORE;

        try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
            if (env.getEnvironment() == null) {
                throw new IllegalStateException("Cannot open " + getPath(partition) + " for store");
            }

            try (Database database = env.getEnvironment().openDatabase(null, getName(), READWRITE_CONFIG)) {

                BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());
                BinaryWrapper value = new BinaryWrapper(binaryEntry.getBinaryValue());

                if (database.put(null, key, value) != OperationStatus.SUCCESS) {
                    logError("store", "to", binaryEntry, partition);
                }
            } catch (Throwable t) {
                logAndThrow("store", "to", binaryEntry, partition, t);
            }
        } finally {
            unlockEnvironment(partition, operation);
        }
    }

    @Override
    public void storeAll(Set set) {
        @SuppressWarnings("unchecked")
        Map<Integer, Collection<BinaryEntry>> entriesByPartition = groupBinaryEntries(set, getManagerContext());

        for (Map.Entry<Integer, Collection<BinaryEntry>> partitionEntries : entriesByPartition.entrySet()) {
            int partition = partitionEntries.getKey();

            Utils.StoreOp operation = Utils.StoreOp.STORE;

            try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {
                if (env.getEnvironment() == null) {
                    throw new IllegalStateException("Cannot open " + getPath(partition) + " for storeAll");
                }

                try (Database database = env.getEnvironment().openDatabase(null, getName(), READWRITE_CONFIG)) {

                    for (BinaryEntry binaryEntry : partitionEntries.getValue()) {
                        BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());
                        BinaryWrapper value = new BinaryWrapper(binaryEntry.getBinaryValue());

                        try {
                            if (database.put(null, key, value) != OperationStatus.SUCCESS) {
                                logError("storeAll", "to", binaryEntry, partition);
                            }
                        } catch (Throwable t) {
                            logAndThrow("storeAll", "to", binaryEntry, partition, t);
                        }
                    }
                }
            } finally {
                GuardSupport.heartbeat();
                unlockEnvironment(partition, operation);
            }
        }
    }

    @Override
    public void erase(BinaryEntry binaryEntry) {
        int partition = getPartitionForEntry(binaryEntry);

        Utils.StoreOp operation = Utils.StoreOp.REMOVE;
        try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {

            if (env.getEnvironment() == null) {
                // no shard for the partition; continue
                return;
            }

            try (Database database = env.getEnvironment().openDatabase(null, getName(), READWRITE_CONFIG)) {
                BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());

                if (database.delete(null, key) != OperationStatus.SUCCESS) {
                    if (logger.isDebugEnabled()) {
                        logError("erase", "from", binaryEntry, partition);
                    }
                }
            } catch (Throwable t) {
                logAndThrow("erase", "from", binaryEntry, partition, t);
            }
        } finally {
            unlockEnvironment(partition, operation);
        }
    }

    @Override
    public void eraseAll(Set set) {
        @SuppressWarnings("unchecked")
        Map<Integer, Collection<BinaryEntry>> entriesByPartition = groupBinaryEntries(set, getManagerContext());

        for (Map.Entry<Integer, Collection<BinaryEntry>> partitionEntries : entriesByPartition.entrySet()) {
            int partition = partitionEntries.getKey();

            Utils.StoreOp operation = Utils.StoreOp.REMOVE;
            try (EnvironmentTuple env = ensureEnvironment(operation, partition)) {

                if (env.getEnvironment() == null) {
                    // no shard for the partition; continue
                    continue;
                }

                try (Database database = env.getEnvironment().openDatabase(null, getName(), READWRITE_CONFIG)) {
                    for (BinaryEntry binaryEntry : partitionEntries.getValue()) {
                        BinaryWrapper key = new BinaryWrapper(binaryEntry.getBinaryKey());

                        try {
                            if (database.delete(null, key) != OperationStatus.SUCCESS) {
                                if (logger.isDebugEnabled()) {
                                    logError("eraseAll", "from", binaryEntry, partition);
                                }
                            }
                        } catch (Throwable t) {
                            logAndThrow("eraseAll", "from", binaryEntry, partition, t);
                        }
                    }
                }
            } finally {
                GuardSupport.heartbeat();
                unlockEnvironment(partition, operation);
            }
        }
    }

    /**
     * The BackingMapManagerContext for this BinaryEntryStore.
     */
    /**
     * Retrieve the configured <tt>BackingMapManagerContext</tt>.
     *
     * @return the BackingMapManagerContext
     */
    protected BackingMapManagerContext getManagerContext() {
        return managerContext;
    }


    /**
     * Logs any Runtime exception thrown during a database <tt>operation</tt>.
     *
     * @param operation   the operation attempted
     * @param direction   the direction of the operation
     * @param binaryEntry the binary entry
     * @param partition   which partition was attempted
     * @param exception   exception thrown
     */
    private void logAndThrow(String operation, String direction, BinaryEntry binaryEntry, int partition,
                                    Throwable exception) {
        logger.error(String.format("Failed %s '%s' %s %s", operation, binaryEntry.getKey(), direction,
                                          getPartitionPath(partition)), exception);
        throw new RuntimeException(exception);
    }

    /**
     * Logs any error during a database <tt>operation</tt>.
     *
     * @param operation   the operation attempted
     * @param direction   the direction of the operation
     * @param binaryEntry the binary entry
     * @param partition   which partition was attempted
     */
    private void logError(String operation, String direction, BinaryEntry binaryEntry, int partition) {
        logger.error(String.format("Failed %s '%s' %s %s", operation, binaryEntry.getKey(), direction,
                                          getPartitionPath(partition)));
    }

    /**
     * Resolves the partition of the specified <tt>binaryEntry</tt>.
     *
     * @param binaryEntry the BinaryEntry to resolve
     * @return the partition which the specified <tt>binaryEntry</tt> belongs to
     */
    private int getPartitionForEntry(BinaryEntry binaryEntry) {
        return getManagerContext().getKeyPartition(binaryEntry.getBinaryKey());
    }

    /**
     * Remove the specified <tt>path</tt> and all sub folders/files.
     *
     * @param path the path to remove
     */
    protected void removePath(Path path) {
        try {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {

                    if (Files.isRegularFile(file)) {
                        Files.delete(file);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                        throws IOException {

                    GuardSupport.heartbeat();
                    Files.deleteIfExists(dir);
                    return FileVisitResult.CONTINUE;
                }
            });

            Files.deleteIfExists(path);
            logger.debug("Removed store " + path);
        } catch (NoSuchFileException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("File already removed " + path);
            }

        } catch (IOException e) {
            logger.error("Cannot remove path " + path, e);
        }
    }
}
