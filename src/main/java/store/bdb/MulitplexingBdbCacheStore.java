/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import com.tangosol.net.BackingMapManagerContext;
import com.tangosol.util.Binary;
import com.tangosol.util.BinaryEntry;
import store.PartitionedBinaryEntryStore;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Set;

/**
 * A PartitionedBinaryEntryStore that can be used to migrate data from one implementation to
 * the other.
 *
 * @author charlie.helin@gmail.com
 */
public class MulitplexingBdbCacheStore
        implements PartitionedBinaryEntryStore {

    private final String name;
    /**
     * The directory where all the partitioned bdb files should be stored.
     */
    private final Path path;

    private final PartitionedBinaryEntryStore readStore;
    private final PartitionedBinaryEntryStore writeStore;

    public MulitplexingBdbCacheStore(BackingMapManagerContext context, String name, String path) {
        this.name = name;
        this.path = FileSystems.getDefault().getPath(path, name);

        readStore = new LegacyBdbCacheStore(context, name, path);
        writeStore = new BdbCacheStore(context, name, path);

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<Binary> getKeys(int partition) {
        return readStore.getKeys(partition);
    }

    @Override
    public boolean remove(int partition) {
        try {
            readStore.remove(partition);
        } finally {
            writeStore.remove(partition);
        }

        return true;
    }

    @Override
    public void destroy() {
        try {
            readStore.destroy();
        } finally {
            writeStore.destroy();
        }
    }

    @Override
    public void load(BinaryEntry binaryEntry) {
        readStore.load(binaryEntry);
    }

    @Override
    public void loadAll(Set set) {
        readStore.loadAll(set);
    }

    @Override
    public void store(BinaryEntry binaryEntry) {
        try {
            readStore.store(binaryEntry);
        } finally {
            writeStore.store(binaryEntry);
        }
    }

    @Override
    public void storeAll(Set set) {
        try {
            readStore.storeAll(set);
        } finally {
            writeStore.storeAll(set);
        }
    }

    @Override
    public void erase(BinaryEntry binaryEntry) {
        try {
            readStore.erase(binaryEntry);
        } finally {
            writeStore.erase(binaryEntry);
        }

    }

    @Override
    public void eraseAll(Set set) {
        try {
            readStore.eraseAll(set);
        } finally {
            writeStore.eraseAll(set);
        }
    }

    @Override
    public String toString() {
        return "BdbCacheStore{" +
                "name='" + name + '\'' +
                ", path=" + path +
                ", readStore=" + readStore +
                ", writeStore=" + writeStore +
                '}';
    }
}
