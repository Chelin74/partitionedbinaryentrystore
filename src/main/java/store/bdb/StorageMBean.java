/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import javax.management.MXBean;

@MXBean
public interface StorageMBean {
    /**
     * Retrieve all the names of the existing Databases handled by this MBean.
     *
     * @return an array of Strings
     */
    String[] getDatabaseNames();

    /**
     * Retrieve all databases which do not have any matching in-memory storage.
     *
     * @return an array of Strings
     */
    String[] getOrphanedDatabases();

    /**
     * removes all the orphan databases, except the ones specified in <tt>excludes</tt>.
     *
     * @param excludes an array of Strings if a database name contains any string, then do not remove that database
     */
    public void removeOrphans(String[] excludes);

    /**
     * Get all the currently configured cache names.
     *
     * @return all the names of caches currently serviced by the CacheService
     */
    String[] getCacheNames();

    /**
     * Remove the specified <tt>database</tt>.
     *
     * @param database name of the database to remove
     */
    void removeDatabase(String database);

    /**
     * Remove the specified <tt>database</tt>, only for partitions owned by this StorageMBean.
     *
     * @param database name of the database to remove
     */
    void removeOwnedDatabase(String database);

    /**
     * Removes the storage for this MBean.
     */
    void remove();

    /**
     * Unlocks all usage for this MBean.
     */
    void unlockAll();
}
