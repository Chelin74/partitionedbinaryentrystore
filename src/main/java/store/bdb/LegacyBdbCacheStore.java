/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.bdb;

import com.tangosol.net.BackingMapManagerContext;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class LegacyBdbCacheStore
        extends AbstractBdbCacheStore {

    /**
     * The directory where all the partitioned bdb files should be stored.
     */
    private final Path path;

    public LegacyBdbCacheStore(BackingMapManagerContext context, String name, String path) {
        super(name, context);
        this.path = FileSystems.getDefault().getPath(path, name);
    }

    @Override
    public String toString() {
        return "LegacyBdbCacheStore{" +
               "managerContext=" + getManagerContext() +
               ", name='" + getName() + '\'' +
               ", path=" + path +
               ", environmentLock=" + environmentLock +
               '}';
    }

    @Override
    protected void onPartitionBroken(int partition) {
        remove(partition);
    }

    @Override
    protected Path getPath(int partition) {
        return path.resolve(String.valueOf(partition));
    }


    @Override
    public boolean remove(int partition) {
        Path path = getPath(partition);

        if (Files.notExists(path)) {
            return false;
        }

        removePath(path);

        return true;
    }

    @Override
    public void destroy() {
        try {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {

                    if (Files.isRegularFile(file)) {
                        Files.delete(file);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                        throws IOException {
                    Files.deleteIfExists(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            // do nothing as destroy can be called concurrently
        }
    }
}
