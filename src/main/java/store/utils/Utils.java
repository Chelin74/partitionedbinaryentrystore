/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store.utils;

import com.tangosol.io.pof.annotation.PortableProperty;
import com.tangosol.io.pof.reflect.SimplePofPath;
import com.tangosol.net.BackingMapManagerContext;
import com.tangosol.util.Binary;
import com.tangosol.util.BinaryEntry;
import com.tangosol.util.ValueExtractor;
import com.tangosol.util.extractor.AbstractExtractor;
import com.tangosol.util.extractor.KeyExtractor;
import com.tangosol.util.extractor.PofExtractor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.tangosol.util.extractor.IdentityExtractor.INSTANCE;

/**
 * User: charlie.helin@gmail.com
 */
public abstract class Utils {
    private static Map<ExtractorPair, ValueExtractor> cache = new ConcurrentHashMap<ExtractorPair, ValueExtractor>();

    public static Map<Integer, Collection<Binary>> groupKeys(Collection<Binary> keys,
                                                             BackingMapManagerContext context) {

        Map<Integer, Collection<Binary>> map = new HashMap<Integer, Collection<Binary>>();

        for (Binary key : keys) {
            if (context.isKeyOwned(key)) {
                int partition = context.getKeyPartition(key);
                Collection<Binary> collKeys = map.get(partition);

                if (collKeys == null) {
                    map.put(partition, collKeys = new HashSet<Binary>());
                }

                collKeys.add(key);
            }
        }

        return map;
    }

    public static Map<Integer, Collection<BinaryEntry>> groupBinaryEntries(Collection<BinaryEntry> entries,
                                                                           BackingMapManagerContext context) {

        Map<Integer, Collection<BinaryEntry>> map = new HashMap<Integer, Collection<BinaryEntry>>();

        for (BinaryEntry entry : entries) {
            Binary key = entry.getBinaryKey();

            if (context.isKeyOwned(key)) {
                int partition = context.getKeyPartition(key);
                Collection<BinaryEntry> collKeys = map.get(partition);

                if (collKeys == null) {
                    map.put(partition, collKeys = new HashSet<BinaryEntry>());
                }

                collKeys.add(entry);
            }
        }

        return map;
    }

    public static Object extractFromEntry(BinaryEntry entry, ValueExtractor extractor, KeyConverter converter) {
        return extractor == null
                ? converter.convert(entry)
                : ((PofExtractor) extractor).extractFromEntry(entry);

    }

    public static ValueExtractor getExtractor(BinaryEntry entry) {
        ValueExtractor extractor = null;
        Class<?> keyClass = entry.getKey().getClass();
        Method method = getKeyProperty(keyClass);

        if (method == null) {
            Field field = getKeyField(keyClass);
            if (field != null) {
                extractor = getKeyExtractor(keyClass, field.getName());
            }
        } else {
            extractor = getKeyExtractor(keyClass, method.getName());
        }
        return extractor;

    }

    private static Method getKeyProperty(Class<?> keyClass) {
        for (Method method : keyClass.getDeclaredMethods()) {
            if (method.getAnnotation(Key.class) == null) {
                continue;
            }
            return method;
        }

        return null;
    }

    private static Field getKeyField(Class<?> keyClass) {
        for (Field field : keyClass.getDeclaredFields()) {
            if (field.getAnnotation(Key.class) == null) {
                continue;
            }
            return field;
        }

        return null;
    }

    public static ValueExtractor getKeyExtractor(Class<?> clazz, String name) {
        if (name == null || name.isEmpty()) {
            return new KeyExtractor(INSTANCE);
        }

        name = ensureFieldName(name);
        ExtractorPair pair = new ExtractorPair(clazz, name);
        ValueExtractor extractor = cache.get(pair);

        if (extractor == null) {
            FieldPath path = getPathForName(clazz, name);
            extractor = new PofExtractor(path.type, new SimplePofPath(path.path), AbstractExtractor.KEY);
            cache.put(pair, extractor);
        }
        return extractor;
    }

    public static ValueExtractor getValueExtractor(Class<?> clazz, String name) {
        if (name == null || name.isEmpty()) {
            return INSTANCE;
        }

        name = ensureFieldName(name);
        ExtractorPair pair = new ExtractorPair(clazz, name);
        ValueExtractor extractor = cache.get(pair);

        if (extractor == null) {
            FieldPath path = getPathForName(clazz, name);
            extractor = new PofExtractor(path.type, new SimplePofPath(path.path), AbstractExtractor.VALUE);
            cache.put(pair, extractor);
        }
        return extractor;
    }

    private static String ensureFieldName(String name) {
        if (name.startsWith("get") || name.startsWith("has")) {
            name = name.substring(3);
        } else if (name.startsWith("is")) {
            name = name.substring(2);
        }
        return Character.toLowerCase(name.charAt(0)) + name.substring(1);
    }

    private static FieldPath getPathForName(Class<?> clazz, String name) {
        String[] parts = name.split("\\.");
        FieldPath path = new FieldPath(clazz, new int[parts.length]);

        for (String part : parts) {
            Field field = getFieldByName(clazz, name);
            if (field == null) {
                throw new IllegalArgumentException("No field " + name);
            }

            PortableProperty property = field.getAnnotation(PortableProperty.class);
            if (property == null) {
                throw new IllegalStateException("No @PortableProperty at " + field.getName());
            }
            path.add(field.getType(), property.value());
            clazz = field.getType();
        }
        return path;
    }

    private static Field getFieldByName(Class<?> clazz, String name) {
        while (!clazz.equals(Object.class)) {
            try {
                return clazz.getDeclaredField(name);

            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }
        return null;
    }

    public enum StoreOp {
        LOAD, STORE, REMOVE
    }

    private static class ExtractorPair {
        private final String name;
        private final Class<?> clazz;

        public ExtractorPair(Class<?> clazz, String name) {
            this.clazz = clazz;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            ExtractorPair that = (ExtractorPair) o;

            if (!clazz.equals(that.clazz)) {
                return false;
            }
            if (!name.equals(that.name)) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + clazz.hashCode();
            return result;
        }
    }

    private static class FieldPath {
        public final int[] path;
        public Class<?> type;
        private int index = 0;

        private FieldPath(Class<?> type, int[] path) {
            this.type = type;
            this.path = path;
        }

        public void add(Class<?> type, int value) {
            this.type = type;
            path[index++] = value;

        }
    }
}
