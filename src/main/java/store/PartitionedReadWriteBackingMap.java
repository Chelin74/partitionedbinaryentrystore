/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store;

import com.tangosol.net.BackingMapManagerContext;
import com.tangosol.net.GuardSupport;
import com.tangosol.net.PartitionedService;
import com.tangosol.net.cache.BinaryEntryStore;
import com.tangosol.net.cache.ReadWriteBackingMap;
import com.tangosol.net.partition.PartitionSet;
import com.tangosol.util.Binary;
import com.tangosol.util.ObservableMap;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class PartitionedReadWriteBackingMap
        extends ReadWriteBackingMap {

    private final Logger logger;
    private final AtomicInteger currentState = new AtomicInteger(State.NOT_INITIALIZED.ordinal());
    /**
     * The PartitionedBinaryEntryStore that this PartitionedReadWriteBackingMap is configured using.
     */
    private final PartitionedBinaryEntryStore store;
    /**
     * Contains all partitions restored now, and historically. If a partition already have been
     * restored it should be available in memory.
     */
    private volatile PartitionSet restoredPartitions;


    public PartitionedReadWriteBackingMap(BackingMapManagerContext ctxService,
                                                 ObservableMap mapInternal, Map mapMisses,
                                                 BinaryEntryStore storeBinary, boolean fReadOnly,
                                                 int cWriteBehindSeconds, double dflRefreshAheadFactor) {
        super(ctxService, mapInternal, mapMisses, storeBinary, fReadOnly, cWriteBehindSeconds, dflRefreshAheadFactor);

        store = (PartitionedBinaryEntryStore) storeBinary;
        restoredPartitions = new PartitionSet(getService().getPartitionCount());
        logger = Logger.getLogger(getClass().getSimpleName() + "[" + store.getName() + "]");
    }

    @Override
    public int size() {
        if (!checkRestored()) {
            restore();
        }
        return super.size();
    }

    @Override
    public Set entrySet() {
        if (!checkRestored()) {
            restore();
        }
        return super.entrySet();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set keySet() {
        if (!checkRestored()) {
            restore();
        }

        return super.keySet();
    }

    @Override
    public Collection values() {
        if (!checkRestored()) {
            restore();
        }

        return super.values();
    }

    @Override
    public boolean containsValue(Object oValue) {
        if (!checkRestored()) {
            restore();
        }
        return super.containsValue(oValue);
    }

    @Override
    public boolean containsKey(Object oKey) {
        if (!checkRestored()) {
            restore();
        }
        return super.containsKey(oKey);
    }

    @Override
    public Object put(Object oKey, Object oValue) {
        if (!checkRestored()) {
            restore();
        }
        return super.put(oKey, oValue);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void putAll(Map m) {
        if (!checkRestored()) {
            restore();
        }

        super.putAll(m);
    }

    @Override
    public Object get(Object oKey) {
        if (!checkRestored()) {
            restore();
        }
        return super.get(oKey);
    }

    @Override
    public Map getAll(Collection colKeys) {
        if (!checkRestored()) {
            restore();
        }
        return super.getAll(colKeys);
    }

    @Override
    public void release() {
        super.release();

        if (!checkStack("onExit")) {
            PartitionSet ownedPartitions = getOwnedPartitions();
            logger.info("Cache " + store.getName() + " will be released " + ownedPartitions);

            for (int partition = ownedPartitions.next(0);
                 partition != -1;
                 partition = ownedPartitions.next(partition + 1)) {
                store.remove(partition);
            }

            PartitionSet partitions = getOwnedPartitions();
            for (int partition = partitions.next(0); partition != -1; partition = partitions.next(partition + 1)) {
                store.remove(partition);
            }

            store.destroy();

            logger.info("Removed " + store.getName() + " store");
        }
    }

    @Override
    public boolean isEmpty() {
        return checkRestored() && super.size() == 0;
    }

    /**
     * Restores all partitions owned by the current member, if there are partitions transfers happening during this
     * restore, they will automatically be picked up.
     * If more than one thread calls this method concurrently, the first one will do the re-population, any subsequent call
     * will block until the first thread has finished populating the cache.
     */
    private void restore() {
        int current = currentState.get();
        String name = store.getName();
        long restored = 0;

        if (current < State.INITIALIZED.ordinal()) {
            if (currentState.compareAndSet(State.NOT_INITIALIZED.ordinal(), State.INITIALIZING.ordinal())) {
                PartitionSet partitions = mask(getOwnedPartitions(), restoredPartitions);

                try {
                    while (!partitions.isEmpty()) {
                        logger.info("Restoring '" + name + "' partitions: " + partitions);
                        restored += restorePartitions(partitions);
                        partitions = mask(getOwnedPartitions(), restoredPartitions);
                    }
                } finally {
                    GuardSupport.heartbeat();
                    currentState.set(State.INITIALIZED.ordinal());
                }

            } else {
                while (currentState.get() != State.INITIALIZED.ordinal()) {
                    try {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Waiting for data "
                                         + name
                                         + "; restore to complete, restored partitions "
                                         + restoredPartitions);
                            if (restoredPartitions.contains(getOwnedPartitions())) {
                                logger.info("Completely restored");
                                currentState.set(State.INITIALIZED.ordinal());
                                return;
                            }
                        }
                        Thread.sleep(5000);
                        GuardSupport.heartbeat();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        throw new RuntimeException(e);
                    }
                }
            }
        }

        logger.info("Restored [" + name + "] " + restored + " items");
    }

    /**
     * Restores all the entries for the specified <tt>partitions</tt>.
     * <p/>
     * This method will record that a partitions have been restored in {@link #restoredPartitions}.
     *
     * @param partitions a PartitionSet where each set bit represents a partition to restore.
     */
    private long restorePartitions(PartitionSet partitions) {
        long restored = 0;
        for (int partition = partitions.next(0); partition != -1; partition = partitions.next(partition + 1)) {
            @SuppressWarnings("unchecked")
            Set<Binary> partitionKeys = getContext().getPartitionKeys(store.getName(), partition);

            try {
                if (partitionKeys == null || partitionKeys.isEmpty()) {
                    partitionKeys = store.getKeys(partition);

                    ensureOwned(partitionKeys);

                    if (!partitionKeys.isEmpty()) {
                        restored += super.getAll(partitionKeys).size();
                    }
                }
            } catch (Throwable t) {
                logger.error("Partition " + partition + " was not properly restored, possible data corruption ", t);
            } finally {
                restoredPartitions.add(partition);
            }
        }
        return restored;
    }

    /**
     * Ensures that all the keys in the specified <tt>key</tt> belongs to the
     * current member. If a key is not owned it will be removed from the set,
     * thus the passed in set may be modified.
     *
     * @param keys the keys which to check
     */
    private void ensureOwned(Set<Binary> keys) {
        BackingMapManagerContext context = getContext();

        for (Iterator<Binary> iterator = keys.iterator(); iterator.hasNext(); ) {
            Binary key = iterator.next();
            if (context.isKeyOwned(key)) {
                continue;
            }

            iterator.remove();
        }
    }

    /**
     * Removes any partition from the <tt>ownedPartitions</tt> set that is already present in the
     * <tt>restoredPartitions</tt> set.
     * Neither set will be modified.
     *
     * @param ownedPartitions    the currently owned partitions
     * @param restoredPartitions the partitions already restored
     * @return a new PartitionSet where only non-restored partitions are flagged
     */
    private PartitionSet mask(PartitionSet ownedPartitions, PartitionSet restoredPartitions) {
        PartitionSet newSet = new PartitionSet(ownedPartitions);
        newSet.remove(restoredPartitions);

        if (!newSet.isEmpty() && logger.isDebugEnabled()) {
            logger.debug("Partitions remaining: " + newSet);
        }

        return newSet;
    }

    /**
     * Check each stack-frame for the presence of any of the specified <tt>methods</tt>.
     *
     * @param methods the method names to check
     * @return true if the stack contains the specified method, otherwise return false
     */
    private boolean checkStack(String... methods) {
        for (StackTraceElement frame : Thread.currentThread().getStackTrace()) {
            for (String method : methods) {
                if (method.equals(frame.getMethodName())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if the current member have restored all the currently owned partitions.
     *
     * @return true iff all owned partitions have been restored, otherwise return false
     */
    private boolean checkRestored() {

        if (checkStack("releasePartition", "instantiateBackingMap", "sendRegister", "putPrimaryResource",
                              "transferPartition", "onExit")) {
            return true;
        } else {

            boolean current = currentState.get() == State.INITIALIZED.ordinal();

            if (current && !mask(getOwnedPartitions(), restoredPartitions).isEmpty()) {
                if (currentState.compareAndSet(State.INITIALIZED.ordinal(), State.NOT_INITIALIZED.ordinal())) {
                    logger.info("Changing state to NOT_INITIALIZED, possibly due to a partition ownership change");
                }
                return false;
            }

            return current;
        }
    }

    /**
     * Get all the currently owned PartitionSet for the current member.
     *
     * @return a PartitionSet where each set bit represents a partition owned by this member
     */
    private PartitionSet getOwnedPartitions() {
        PartitionedService service = getService();
        return service.getOwnedPartitions(service.getCluster().getLocalMember());
    }

    /**
     * Get the PartitionedService.
     *
     * @return the PartitionedService
     */
    private PartitionedService getService() {
        return (PartitionedService) getCacheService();
    }

    private enum State {
        NOT_INITIALIZED, INITIALIZING, INITIALIZED
    }
}
