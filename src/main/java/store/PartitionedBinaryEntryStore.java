/*
 *
 *  * Copyright 2011 Charlie Helin (charlie.helin@gmail.com)
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package store;

import com.tangosol.net.cache.BinaryEntryStore;
import com.tangosol.util.Binary;

import java.util.Set;

/**
 * User: charlie.helin@gmail.com
 */
public interface PartitionedBinaryEntryStore
        extends BinaryEntryStore {

    /**
     * For logging purposes, gets the name of the Store.
     *
     * @return the name of the store.
     */
    String getName();

    /**
     * Get all the keys for the specified <tt>partition</tt>.
     *
     * @param partition the partition to resolve all the keys for
     * @return a set of Binary keys
     */
    Set<Binary> getKeys(int partition);

    /**
     * Remove the storage for the specified <tt>partition</tt>.
     *
     * @param partition the partition which to remove any associated resources
     */
    boolean remove(int partition);

    /**
     * Called when this store will no longer be valid, erase all associated resource.
     */
    void destroy();
}
